<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18.10.2018
 * Time: 23:12
 */

namespace Api;


class User extends AbstractResult
{

    public $id;
    public $username;
    public $full_name;
    public $profile_picture;
    public $website;
    public $is_business;
    public $mediaCount;
    public $accountType;

    public function parseResult($result)
    {
        $this->username = $result->username;
        $this->id = $result->id;
        $this->mediaCount = $result->media_count;
        $this->accountType = $result->account_type;
    }



}