<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18.10.2018
 * Time: 21:30
 */

namespace Api;


use Api\Results\RecentMediaResult;

class Instagram
{


    protected $accessToken;
    protected $clientId;


    protected $clientSecret;


    protected $userName;
    protected $userId;

    protected $callbackUrl="";
    protected $code;

    protected $userFactory;

    public function __construct()
    {
        $this->userFactory = new UserFactory();
    }

    public function setUserFactory(UserFactory $userFactory)
    {
        $this->userFactory = $userFactory;
    }

    /**
     * @return mixed
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param mixed $userName
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }



    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }




    /**
     * @return string
     */
    public function getCallbackUrl(): string
    {
        return $this->callbackUrl;
    }

    /**
     * @param string $callbackUrl
     */
    public function setCallbackUrl(string $callbackUrl)
    {
        $this->callbackUrl = $callbackUrl;
    }

    public function getRecentMediaUrl($accessToken)
    {
        return "https://graph.instagram.com/me/media?fields=media_type,timestamp,media_url,permalink,thumbnail_url,caption&access_token=$accessToken";
        //old
//        return "https://api.instagram.com/v1/users/self/media/recent?access_token=$accessToken";
    }

    public function getUserProfileUrl($accessToken)
    {
        return "https://graph.instagram.com/me?fields=id,username,account_type,media_count&access_token=$accessToken";
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param mixed $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return mixed
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @param mixed $clientSecret
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }

    public function selfMediaUrl()
    {
        return "https://api.instagram.com/v1/users/self/?access_token=$this->accessToken";
    }

    public function getAuthUrl()
    {
        $clientId = $this->clientId;
        $callbackUri = $this->callbackUrl;

        return "https://api.instagram.com/oauth/authorize?app_id=$clientId&redirect_uri=".($callbackUri)."&scope=user_profile,user_media&response_type=code";

        //old api
        //        return "https://api.instagram.com/oauth/authorize/?client_id=$clientId&response_type=code&redirect_uri=$callbackUri";
    }

    public function getAccessTokenRequestUrl()
    {

        return "https://api.instagram.com/oauth/access_token";

    }

    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }


    public function initCurl($url)
    {
        $client = curl_init();
        curl_setopt($client,CURLOPT_URL, $url);
        curl_setopt($client, CURLOPT_RETURNTRANSFER, TRUE);
        return $client;
    }


    public function curlGetRequest($url)
    {

        $client = $this->initCurl($url);
        $result = curl_exec($client);
        curl_close($client);

        return $result;

    }

    public function curlPostRequest($url, $postString)
    {
        $client = $this->initCurl($url);
        curl_setopt($client, CURLOPT_POST, true);
        curl_setopt($client, CURLOPT_POSTFIELDS, $postString);

        $result = curl_exec($client);
        curl_close($client);

        return $result;
    }

    public function curlDecodedGetRequest($url)
    {
        return json_decode($this->curlGetRequest($url));
    }

    public function curlDecodedPostRequest($url)
    {
        return json_decode($this->curlPostRequest($url));
    }

    public function getRecentMedia()
    {
        $url = $this->getRecentMediaUrl($this->accessToken);

        $result = $this->curlDecodedGetRequest($url);

        $implementation = new RecentMediaResult();
        $implementation->setError($result);
        $implementation->setImplementationData($result);
        return $implementation;
    }

    public function getUserProfile()
    {
        $url = $this->getUserProfileUrl($this->accessToken);
        $result = $this->curlDecodedGetRequest($url);
        $user = $this->userFactory->make();
        $user->setError($result);
        $user->setImplementationData($result);
        return $user;
    }

    public function generateAccessToken()
    {

        $url = $this->getAccessTokenRequestUrl();
//        $postString = "client_id=$this->clientId&code=$this->code&client_secret=$this->clientSecret&grant_type=authorization_code&redirect_uri=$this->callbackUrl";
        $postString = "app_id=$this->clientId&app_secret=$this->clientSecret&grant_type=authorization_code&redirect_uri=".($this->callbackUrl)."&code=$this->code";
//        dd($this->code);
        $result = $this->curlPostRequest($url,$postString);

        try
        {
            $decoded = json_decode($result);
        }
        catch (\Exception $exception)
        {
            throw new \Exception('error while parsing json');
        }

        return $decoded->access_token;
    }



}
