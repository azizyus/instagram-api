<?php


namespace Api;


abstract class AbstractResult
{

    public $error;

    public function setError($result)
    {
        $this->error = new ExceptionHandler();
        if(isset($result->error)) $this->error->setError($result);
    }


    public function setImplementationData($result)
    {
       if(!$this->error->hasError) $this->parseResult($result);
    }

    abstract public function parseResult($result);

}