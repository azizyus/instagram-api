<?php

namespace Api;


class ExceptionHandler
{

    public $message = null;
    public $type = null;
    public $error_data=[];
    public $fbtrace_id = null;
    public $code = null;
    public $hasError = false;
    public function setError($result)
    {
        $this->message =  $result->error->message;
        $this->type = $result->error->type;
        //$this->error_data = $result->error->error_data; //not always
        $this->code = $result->error->code;
        $this->fbtrace_id = $result->error->fbtrace_id;
        $this->hasError = true;
    }

}