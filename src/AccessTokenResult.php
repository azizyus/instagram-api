<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 18.10.2018
 * Time: 23:11
 */

namespace Api;


class AccessTokenResult
{


    public $accessToken;
    public $user;
    public $error;
    public $userId;
    public function __construct(string $resultJson)
    {

        $result = json_decode( $resultJson,false);


        $this->error = new ExceptionHandler();



        if(isset($result->error_type))
        {

            $this->error->setError($result);


        }
        else
        {

            /**
             * @var $user User
             */

//            $user = $result->user;
//            $this->user = $user;
            $this->userId = $result->user_id;
            $this->accessToken = $result->access_token;
        }



    }

}