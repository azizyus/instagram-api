<?php


namespace Api\Results;


class Media
{

    public $id;
    public $mediaUrl;
    public $mediaPermanentUrl;
    public $mediaType;
    public $timestamp;

    public $link;

    //    public $mediaThumbnailUrl;

    public function __construct($media)
    {
        $this->id = $media->id;
        $this->mediaUrl = $media->media_url;
        $this->mediaPermanentUrl = $media->permalink;
        $this->mediaType = $media->media_type;
        $this->timestamp = $media->timestamp;
        $this->link = $this->mediaUrl;
//        $this->mediaThumbnailUrl = $media->thumbnail_url;
    }

    public function __get($name)
    {
        if(in_array($name,["images","low_resolution"]))
        {
            return $this;
        }
        elseif(in_array($name,["url"]))
        {
            return $this->mediaUrl;
        }

        return $this->$name;
    }


}