<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 19.10.2018
 * Time: 00:03
 */

namespace Api\Results;


use Api\AbstractResult;
use Api\ExceptionHandler;

class RecentMediaResult extends AbstractResult
{

    //the image array you should foreach but it could be return null or something you can iterate
    //so be sure its an array like a => if(isset($result->data)) $this->data = $result->data;
    public $data=[];

    public function parseResult($result)
    {
        if(isset($result->data) && is_array($result->data))
        {
            $this->data = array_map(function($media){
                return new Media($media);
            },$result->data);
        }
    }

}