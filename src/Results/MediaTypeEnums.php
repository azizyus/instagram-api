<?php


namespace Api\Results;


class MediaTypeEnums
{


    const _VIDEO="VIDEO";
    const _IMAGE="IMAGE";
    const _CAROUSEL_ALBUM="CAROUSEL_ALBUM";

}